%{
#include <stdio.h>
%}
%token IDENTIFICADOR PRT SCF OPAS TP CONSTENTERA COM NL TRO MAS POR DIV APAR CPAR 
%start instrucciones
%%
instrucciones : instrucciones instruccion
			  | instruccion
			  ;
instruccion : IDENTIFICADOR OPAS expresion NL
			| PRT valida NL
			| SCF valida NL 
			| TRO termino instruccion NL
			;
expresion : termino
		  | expresion MAS termino
		  | expresion POR termino
		  | expresion DIV termino
		  ;
termino : expresion 
		| IDENTIFICADOR 
        | CONSTENTERA 
		;
valida : APAR COM expresion COM CPAR
	   ;
%%

char *s;
yyerror(s) 
{
printf ("Morite: %s\n", s);
}

int main(){
yyparse();
return 0;
}
  